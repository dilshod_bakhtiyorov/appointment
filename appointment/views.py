from rest_framework import viewsets
from datetime import datetime
from .models import Appointment
from .serializers import AppointmentSerializer
from custom_permission.permission import CustomerPermission
from provider.models import Provider
from customer.models import Customer
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.views import APIView
from availability.filters import AvailabilityFilter
from rest_framework.generics import ListAPIView
import json
from .test import get_slots


class AppointmentModelViewSet(viewsets.ModelViewSet):
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer
    permission_classes = (CustomerPermission,)
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['date']

    def create(self, request, *args, **kwargs):
        data = request.data
        date = data['date']
        user_id = request.user.id
        customer_id = Customer.objects.get(user__id__contains=user_id).id
        data['customer'] = customer_id
        serializer = AppointmentSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)


def convert_to_tuple(value):
    my_list = []
    for i in value:
        x = datetime.strptime(i, "%Y:%m:%d %H:%M:%S")
        my_list.append(x)
    lst_tuple = [x for x in zip(*[iter(my_list)]*2)]
    return lst_tuple


class AvailabilityTimeSlot(APIView):
    def post(self, request, *args, **kwargs):
        data = request.data
        provider_appointments_time = data['provider_appointments_time']
        provider_time_slots = data['provider_time_slots']
        is_break_time = data['is_break_time']
        provider_appointments_time = provider_appointments_time + is_break_time
        my_list = []
        for appointment in provider_appointments_time:
            for i in appointment:
                x = datetime.strptime(i, "%Y:%m:%d %H:%M:%S")
                my_list.append(x)
            lst_tuple = [x for x in zip(*[iter(my_list)]*2)]
        list_slots = []
        for slot in provider_time_slots:
            for p in slot:
                x = datetime.strptime(p, "%Y:%m:%d %H:%M:%S")
                list_slots.append(x)
        tuple_slots = tuple(list_slots)
        data = get_slots(tuple_slots, lst_tuple)
        return Response({'avilability time slots': data})
