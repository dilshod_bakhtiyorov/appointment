from datetime import timedelta
import datetime


def get_slots(hours, appointments, duration=timedelta(hours=0.5)):
    data = []
    my_list = []
    x = len(hours)
    for i in range(0, x):
        my_list.append((hours[i], hours[i]))
    slots = sorted(my_list + appointments)
    for start, end in ((slots[i][1], slots[i+1][0]) for i in range(len(slots)-1)):
        assert start <= end, "Cannot attend all appointments"
        while start + duration <= end:
            data.append("{:%H:%M} - {:%H:%M}".format(start, start + duration))
            start=start + duration
    return data


