from rest_framework import filters
from django.db.models import Q
from customer.models import Customer
from provider.models import Provider


def convert_query_params_to_dict(query_params):
    data = {

    }
    data['day'] = query_params.get('day', None)
    data['date'] = query_params.get('date', None)
    data['is_break'] = query_params.get('is_break', None)
    data['is_working'] = query_params.get('is_working', None)
    return data



class AvailabilityFilter(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        provider_id = view.kwargs['provider_id']
        provider_id = Provider.objects.get(id=provider_id).id
        data = convert_query_params_to_dict(request.query_params)
        data = {k: v for k, v in data.items() if v is not None}
        data['provider_id']=provider_id
        return queryset.filter(**data)